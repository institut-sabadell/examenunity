using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cuadrado;
    public NBloques numeroBloques; // Contiene un public con el N de bloques

    void Start()
    {
        for (int i=0; i < numeroBloques.Value; i++)
        {
            GameObject newCuadrado = Instantiate(cuadrado);
            newCuadrado.transform.position = new Vector2(Random.Range(-9.6f,9.6f), Random.Range(0, 4));
            newCuadrado.name = "Bloc "+i;
        }
    }




    /*
     *     public GameObject[] gameObjects;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnejar());


    }

    public IEnumerator spawnejar()
    {
        while (true)
        {
            int rand = Random.Range(0, 2);
            GameObject newGameObject = Instantiate(gameObjects[rand]);
            float randX = Random.Range(-8.5f, 5);
            newGameObject.transform.position = new Vector2(randX, 2);

            yield return new WaitForSeconds(1.5f);//trivial

        }
    }
     */


}
