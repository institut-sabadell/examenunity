using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenes : MonoBehaviour
{
    public GameObject Label;
    public NBloques numeroBloques;
    // Start is called before the first frame update
    public void Escena()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void opciones(int a)
    {
        if (Label.GetComponent<TextMeshProUGUI>().text == "5 blocs")
        {
            numeroBloques.Value = 5;
        } 
        else if (Label.GetComponent<TextMeshProUGUI>().text == "10 blocs")
        {
            numeroBloques.Value = 10;
        } 
        else if (Label.GetComponent<TextMeshProUGUI>().text == "15 blocs")
        {
            numeroBloques.Value = 15;
        }
        print(Label.GetComponent<TextMeshProUGUI>().text);
    }
}
