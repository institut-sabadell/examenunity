using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloqueScript : MonoBehaviour
{

    public GameObject powerup;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag!="bloque") // Si le da la pelota...
        {
            if (Random.Range(0,2)==0)
            {
                GameObject newPowerup = Instantiate(powerup);
                newPowerup.transform.position = this.transform.position; // Que el nuevo PowerUp aparezca en la misma posicion del bloque
                int randColor = Random.Range(0, 4);
                if (randColor==0)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color=Color.red;
                }
                else if (randColor == 1)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.blue;
                } 
                else if (randColor == 2)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.yellow;
                }
                else
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.green;
                }
            }
            
            Destroy(this.gameObject);
        }

        /*      --- SCRIPT BLOQUE
         *      
         *     private void OnDestroy()
        { 
        if (power != null) POWER es un ScriptableObject
        {
            int numAletaroio = Random.Range(0, 2);
            Debug.Log(numAletaroio);
            if(numAletaroio == 0)
            {
                Instantiate(power.bonus, transform.position, Quaternion.identity); // Poniendo en la pelota destroy(collision.gameobject)
            }
          
            
        }
        }

        --- SCRIPT SCRIPTABLEOBJECT POWERUP

        [CreateAssetMenu(fileName = "NewPowerUp", menuName = "PowerUp")]
        public class powerUp : ScriptableObject
        {
        public GameObject bonus; Y colocamos el prefab de bonus
        }


        ---- SCRIPT ASOCIADO AL PREFAB DEL BONUS

            void Start()
        {
        int numeroAleatorio = Random.Range(0, 3);
        if (numeroAleatorio == 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }else if(numeroAleatorio == 1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else if(numeroAleatorio == 2)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
       
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
        Debug.Log("holla");
        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            Debug.Log("Habra bonus");
            Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().color);
        }
        else if(collision.gameObject.tag =="suelo")
        {
           
            Destroy(gameObject);
        }

        }

         */

    }
    private void OnMouseDown()
    {
        print(this.gameObject.name);
        Destroy(this.gameObject);
    }
}
